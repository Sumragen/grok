import { DoCheck, OnInit } from '@angular/core';
import { BuilderService } from '../../../builder/builder.service';
import { BuilderMode } from '../../models/builderMode';

export abstract class AbstractSideMenuComponent implements DoCheck {
  isVisible: boolean;

  constructor(private builderService: BuilderService) {
  }

  ngDoCheck() {
    this.isVisible = this.builderService.getBuilderMode() === BuilderMode.CONSTRUCTOR;
  }
}
