export interface Field {
  component: any;
  _ref?: any;
  inputs: {
    id: string;
    name?: string;
    label?: string;
    type?: string;
    styles?: object;
  };
}
