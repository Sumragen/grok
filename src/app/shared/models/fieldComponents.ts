import { InputTextComponent } from '../../builder/fields/input-text/input-text.component';
import { InputNumberComponent } from '../../builder/fields/input-number/input-number.component';
import { InputPhoneComponent } from '../../builder/fields/input-phone/input-phone.component';
import { InputCreditCardComponent } from '../../builder/fields/input-credit-card/input-credit-card.component';
import { InputSumComponent } from '../../builder/fields/input-sum/input-sum.component';
import { DropdownComponent } from '../../builder/fields/dropdown/dropdown.component';
import { RadioComponent } from '../../builder/fields/radio/radio.component';
import { CheckboxComponent } from '../../builder/fields/checkbox/checkbox.component';

export const FIELD_COMPONENTS = {
  INPUT_CREDIT_CARD: InputCreditCardComponent,
  INPUT_NUMBER: InputNumberComponent,
  INPUT_PHONE: InputPhoneComponent,
  INPUT_TEXT: InputTextComponent,
  INPUT_SUM: InputSumComponent,
  DROPDOWN: DropdownComponent,
  CHECKBOX: CheckboxComponent,
  RADIO: RadioComponent
};
