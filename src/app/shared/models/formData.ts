import { Field } from './field';

export interface FormData {
  title: string;
  backgroundColor: string;
  formColor: string;
  fields: Field[];
}
