import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { FirebaseObjectObservable, FirebaseListObservable } from 'angularfire2/database-deprecated';
import {environment} from '../../../environments/environment';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Injectable()
export class DbService {
  constructor(private fireDatabase: AngularFireDatabase, private http: HttpClient) {
  }

  public statuses: FirebaseListObservable<any[]>;
  getForm(id: string) {
    // return this.fireDatabase.list(`/${id}`).valueChanges();
    // return this.statuses = this.fireDatabase.list('/4').valueChanges()
    //   .map(arr => arr.reverse()) as FirebaseListObservable<any[]>;
    return this.http.get(`https://grok-form-builder.firebaseio.com/.json`)
      .map(data => {
        return data[id];
      });
  }

  createForm(id: string, schema: string) {
    const cursor = this.fireDatabase.object('/' + id);
    return cursor.set(schema);
  }
}
