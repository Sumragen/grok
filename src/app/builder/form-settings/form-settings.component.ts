import { Component, OnInit } from '@angular/core';
import { BuilderService } from '../builder.service';
import { FormData } from '../../shared/models/formData';
import { AbstractSideMenuComponent } from '../../shared/components/side-menu/side-menu.component';

@Component({
  selector: 'app-form-settings',
  templateUrl: './form-settings.component.html',
  styleUrls: ['./form-settings.component.sass']
})
export class FormSettingsComponent extends AbstractSideMenuComponent implements OnInit {
  isVisible = true;
  form: FormData;
  formColor: string;
  backgroundColor: string;
  title: string;

  constructor(private builderServiceInstanse: BuilderService) {
    super(builderServiceInstanse);
  }

  ngOnInit() {
    this.form = this.builderServiceInstanse.getFormData();
    this.formColor = this.form.formColor;
    this.backgroundColor = this.form.backgroundColor;
    this.title = this.form.title;
  }

  formColorChanged(event) {
    this.builderServiceInstanse.setFormColor(this.formColor);
  }

  backgroundColorChanged(event) {
    this.builderServiceInstanse.setBackgroundColor(this.backgroundColor);
  }

  updateFormTitle(event) {
    this.builderServiceInstanse.setFormTitle(this.title);
  }
}
