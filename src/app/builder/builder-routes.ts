import { Routes } from '@angular/router';
import { FormReaderComponent } from './form-reader/form-reader.component';
import { BuilderComponent } from './builder.component';

export const builderRoutes: Routes = [
  {
    path: '',
    component: BuilderComponent
  },
  {
    path: ':id',
    component: FormReaderComponent
  }
];
