import { Component, OnDestroy, OnInit } from '@angular/core';
import { BuilderMode } from '../shared/models/builderMode';
import { BuilderService } from './builder.service';
import { Subscription } from 'rxjs/Subscription';
import { DbService } from '../shared/services/db.service';

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.sass'],
  providers: [BuilderService]
})
export class BuilderComponent implements OnInit, OnDestroy {
  public isInBuilderMode: boolean;
  public modes;
  backgroundColor: string;
  private backgroundColorSubscription: Subscription;

  constructor(private builderService: BuilderService, private dbService: DbService) {
  }

  ngOnInit() {
    this.isInBuilderMode = false;
    this.modes = [
      {label: 'Constructor', value: 0},
      {label: 'Preview', value: 1}
    ];
    this.backgroundColorSubscription = this.builderService.backgroundColorChanged
      .subscribe((color: string) => this.backgroundColor = color);
  }

  ngOnDestroy() {
    this.backgroundColorSubscription.unsubscribe();
  }

  changeMode(event) {
    let mode;
    this.isInBuilderMode = event.value === 0;
    mode = this.isInBuilderMode ? BuilderMode.CONSTRUCTOR : BuilderMode.PREVIEW;
    this.builderService.changeMode(mode);
  }

  isFieldSelected(): boolean {
    return !!this.builderService.getSelectedField();
  }

  getSelectedField(): any {
    return this.builderService.getSelectedField();
  }

  deployForm() {
    this.dbService.createForm('id' + Math.floor(Math.random() * 1000), JSON.stringify(this.builderService.getFormData()));
  }
}
