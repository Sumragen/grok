import { Field } from '../shared/models/field';
import { FormData } from '../shared/models/formData';
import { BuilderMode } from '../shared/models/builderMode';
import { FIELD_COMPONENTS } from '../shared/models/fieldComponents';
import { Injectable } from '@angular/core';

import { BasicInputComponent } from './fields/basic-field.component';
import { TextFieldComponent } from './fields/text-field/text-field.component';
import { InputTextComponent } from './fields/input-text/input-text.component';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class BuilderService {
  private _formData: FormData = {
    title: 'Test',
    backgroundColor: '#f2f2f2',
    formColor: '#f2f2f2',
    fields: []
  };
  private _builderMode: BuilderMode;
  // TODO: remove 'any' typings
  private _draggedField: string;
  private _selectedField: any;
  public formColorChanged = new Subject();
  public formTitleChanged = new Subject();
  public backgroundColorChanged = new Subject();


  getFormData(): FormData {
    const form: any = {};
    form.title = this._formData.title;
    form.backgroundColor = this._formData.backgroundColor;
    form.formColor = this._formData.formColor;
    form.fields = this._formData.fields.map(field => {
      return {
        inputs: field.inputs
      };
    });
    return form;
  }

  setFormData(data: any) {
    this._formData = data;
  }

  addRefToFieldById(id: string, ref: any) {
    const field = this.findFieldById(id);
    field._ref = ref;
  }

  findFieldById(id) {
    return this._formData.fields.find(value => {
      return value.inputs.id === id;
    });
  }

  updateFieldById(id: string, field: any) {
    let fieldI = this.findFieldById(id);
    fieldI = field;
  }

  attachField(field: Field) {
    this._formData.fields.push(field);
  }

  rewriteFields(fields: Field[]) {
    this._formData.fields = fields;
  }

  getFields() {
    return this._formData.fields;
  }

  getSelectedField() {
    return this._selectedField;
  }

  setSelectedField(field: any) {
    this._selectedField = field;
  }

  getBuilderMode(): BuilderMode {
    return this._builderMode;
  }

  changeMode(mode: BuilderMode) {
    this._builderMode = mode;
  }

  setDraggedField(field: string): void {
    this._draggedField = field;
  }

  getDraggedField(): string {
    return this._draggedField;
  }

  setBasicInputComponent(component: any, id?: string, name?: string, label?: string, styles?: any): any {
    return {
      component: component,
      inputs: {
        id: id || '' + Math.floor(Math.random() * 1000),
        name: name || '',
        label: label || 'base',
        styles: styles || ''
      }
    };
  }

  getFormColor(): string {
    return this._formData.formColor;
  }

  setFormColor(color: string) {
    this._formData.formColor = color;
    this.formColorChanged.next(color);
  }

  setBackgroundColor(color: string) {
    this._formData.backgroundColor = color;
    this.backgroundColorChanged.next(color);
  }

  setFormTitle(title: string) {
    this._formData.title = title;
    this.formTitleChanged.next(title);
  }

  getFormTitle(): string {
    return this._formData.title;
  }
}
