import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { BuilderService } from '../builder.service';
import { FIELD_COMPONENTS } from '../../shared/models/fieldComponents';
import { Field } from '../../shared/models/field';
import { DbService } from '../../shared/services/db.service';

@Component({
  selector: 'app-form-reader',
  templateUrl: './form-reader.component.html',
  styleUrls: ['./form-reader.component.sass']
})
export class FormReaderComponent implements OnInit, OnDestroy {
  @Input() isInBuilderMode: boolean;
  formId: string;
  formTitle: string;
  formColor: string;
  private subscription: Subscription;
  private formColorSubscription: Subscription;
  private formTitleSubscription: Subscription;

  constructor(private route: ActivatedRoute, private builderService: BuilderService, private db: DbService) {
  }

  ngOnInit() {
    this.isInBuilderMode = false;
    this.formColor = this.builderService.getFormColor();
    this.formTitle = this.builderService.getFormTitle();
    this.subscription = this.route.params.subscribe(params => {
      this.formId = params['id'];
      if (!!this.formId) {
        this.db.getForm(this.formId)
          .subscribe((data) => {
            this.builderService.setFormData(data);
          });
      }
    });
    this.formColorSubscription = this.builderService.formColorChanged
      .subscribe((color: string) => this.formColor = color);
    this.formTitleSubscription = this.builderService.formTitleChanged
      .subscribe((title: string) => this.formTitle = title);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.formColorSubscription.unsubscribe();
    this.formTitleSubscription.unsubscribe();
  }

  selectField(field: any) {
    this.builderService.setSelectedField(field);
  }

  isFormIdExist(): boolean {
    return this.formId !== undefined;
  }

  onDrop(event: DragEvent) {
    const componentName = this.builderService.getDraggedField();
    this.pushField(componentName);
    this.builderService.setDraggedField(null);
  }

  getFields(): Field[] {
    return this.builderService.getFields();
  }

  private pushField(fieldTypeName: string) {
    const component = FIELD_COMPONENTS[fieldTypeName];
    const field = this.builderService.setBasicInputComponent(component);
    this.builderService.attachField(field);
  }
}
