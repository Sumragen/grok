import {Component, Injector} from '@angular/core';
import {BasicInputComponent} from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-text-field',
  templateUrl: './text-field.component.html'
})
export class TextFieldComponent extends BasicInputComponent {

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }
}
