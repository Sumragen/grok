import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.sass']
})
export class CheckboxComponent extends BasicInputComponent {
  public value = '';
  public label = '';

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

}
