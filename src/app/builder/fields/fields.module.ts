import { NgModule } from '@angular/core';
import { InputTextComponent } from './input-text/input-text.component';
import { CheckboxModule, DropdownModule, InputMaskModule, InputTextModule, RadioButtonModule } from 'primeng/primeng';
import { InputNumberComponent } from './input-number/input-number.component';
import { InputPhoneComponent } from './input-phone/input-phone.component';
import { InputCreditCardComponent } from './input-credit-card/input-credit-card.component';
import { InputSumComponent } from './input-sum/input-sum.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { RadioComponent } from './radio/radio.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

@NgModule({
  declarations: [
    InputCreditCardComponent,
    InputNumberComponent,
    InputPhoneComponent,
    InputTextComponent,
    CheckboxComponent,
    InputSumComponent,
    DropdownComponent,
    RadioComponent
  ],
  imports: [
    RadioButtonModule,
    InputTextModule,
    InputMaskModule,
    DropdownModule,
    CheckboxModule
  ],
  entryComponents: [
    InputCreditCardComponent,
    InputNumberComponent,
    InputPhoneComponent,
    InputTextComponent,
    CheckboxComponent,
    InputSumComponent,
    DropdownComponent,
    RadioComponent
  ],
  exports: [
    InputCreditCardComponent,
    InputNumberComponent,
    InputPhoneComponent,
    InputTextComponent,
    CheckboxComponent,
    InputSumComponent,
    DropdownComponent,
    RadioComponent
  ]
})
export class FieldsModule {
}
