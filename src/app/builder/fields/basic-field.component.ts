import { Component, Input, Injector } from '@angular/core';
import { BuilderService } from '../builder.service';

@Component({
  selector: 'app-basic-input',
  template: ' '
})

export class BasicInputComponent {
  @Input() id: string;
  @Input() name: string;
  @Input() label: string;

  constructor(public injector: Injector, private builderService: BuilderService) {
    this.id = this.injector.get('id');
    this.name = this.injector.get('name');
    this.label = this.injector.get('label');
  }

  getLabel(): string {
    return this.builderService.findFieldById(this.id).inputs.label;
  }

}
