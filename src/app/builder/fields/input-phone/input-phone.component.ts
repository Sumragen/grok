import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-input-phone',
  templateUrl: './input-phone.component.html',
  styleUrls: ['./input-phone.component.sass']
})
export class InputPhoneComponent extends BasicInputComponent implements OnInit {
  public mask: string;

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

  ngOnInit() {
    this.mask = '(999) 99-99-999';
  }

}
