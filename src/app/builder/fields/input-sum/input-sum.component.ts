import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-input-sum',
  templateUrl: './input-sum.component.html',
  styleUrls: ['./input-sum.component.sass']
})
export class InputSumComponent  extends BasicInputComponent {

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

}
