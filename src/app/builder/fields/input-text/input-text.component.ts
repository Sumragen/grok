import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.sass']
})
export class InputTextComponent extends BasicInputComponent {

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

}
