import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-input-number',
  templateUrl: './input-number.component.html',
  styleUrls: ['./input-number.component.sass']
})
export class InputNumberComponent extends BasicInputComponent {

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

}
