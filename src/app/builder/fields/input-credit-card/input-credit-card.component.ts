import { Component, Injector, OnInit } from '@angular/core';
import { BasicInputComponent } from '../basic-field.component';
import { BuilderService } from '../../builder.service';

@Component({
  selector: 'app-input-credit-card',
  templateUrl: './input-credit-card.component.html',
  styleUrls: ['./input-credit-card.component.sass']
})
export class InputCreditCardComponent  extends BasicInputComponent {

  constructor(injector: Injector, private builderServiceInstance: BuilderService) {
    super(injector, builderServiceInstance);
  }

}
