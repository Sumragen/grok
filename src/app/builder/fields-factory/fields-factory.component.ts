import {
  Component,
  Input,
  ViewContainerRef,
  ViewChild,
  ReflectiveInjector,
  ComponentFactoryResolver
} from '@angular/core';

import { TextFieldComponent } from '../fields/text-field/text-field.component';
import { BuilderService } from '../builder.service';

@Component({
  selector: 'app-fields-factory',
  templateUrl: './fields-factory.component.html',
  entryComponents: [TextFieldComponent]
})
export class FieldsFactoryComponent {
  currentComponent = null;
  @ViewChild('componentContainer', {read: ViewContainerRef}) componentContainer: ViewContainerRef;

  @Input()
  set componentData(data: { component: any, inputs: any }) {
    if (!data) {
      return;
    }

    const inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {provide: inputName, useValue: data.inputs[inputName]};
    });
    const resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    const injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.componentContainer.parentInjector);
    const factory = this.resolver.resolveComponentFactory(data.component);
    const component = factory.create(injector);
    this.builderService.addRefToFieldById(data.inputs.id, component);

    this.componentContainer.insert(component.hostView);

    if (this.currentComponent) {
      this.currentComponent.destroy();
    }

    this.currentComponent = component;
  }

  constructor(private resolver: ComponentFactoryResolver, private builderService: BuilderService) {
  }

}
