import { Component, OnInit } from '@angular/core';
import { AbstractSideMenuComponent } from '../../shared/components/side-menu/side-menu.component';
import { BuilderService } from '../builder.service';
import { FIELD_COMPONENTS } from '../../shared/models/fieldComponents';

@Component({
  selector: 'app-fielder',
  templateUrl: './fielder.component.html',
  styleUrls: ['./fielder.component.sass']
})
export class FielderComponent extends AbstractSideMenuComponent {
  isVisible = true;

  constructor(private builderServiceInstance: BuilderService) {
    super(builderServiceInstance);
  }

  onDragStart(event: DragEvent, fieldName: string) {
    this.builderServiceInstance.setDraggedField(fieldName);
  }
}
