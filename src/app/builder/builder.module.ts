import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import {
  AccordionModule, ButtonModule, ColorPickerModule, DragDropModule, PanelModule,
  SelectButtonModule
} from 'primeng/primeng';

import { FielderComponent } from './fielder/fielder.component';
import { FormReaderComponent } from './form-reader/form-reader.component';
import { BuilderComponent } from './builder.component';
import { BuilderService } from './builder.service';
import { FormReaderService } from './form-reader/form-reader.service';
import { builderRoutes } from './builder-routes';
import { FieldEditorComponent } from './field-editor/field-editor.component';
import { FormSettingsComponent } from './form-settings/form-settings.component';
import { BasicInputComponent } from './fields/basic-field.component';
import { TextFieldComponent } from './fields/text-field/text-field.component';
import { FieldsFactoryComponent } from './fields-factory/fields-factory.component';
import { FieldsModule } from './fields/fields.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    BuilderComponent,
    FormReaderComponent,
    FielderComponent,
    FieldEditorComponent,
    FormSettingsComponent,
    BasicInputComponent,
    TextFieldComponent,
    FieldsFactoryComponent
  ],
  imports: [
    FormsModule,
    RouterModule.forChild(builderRoutes),
    CommonModule,
    FormsModule,
    PanelModule,
    BrowserAnimationsModule,
    DragDropModule,
    AccordionModule,
    ButtonModule,
    FieldsModule,
    SelectButtonModule,
    ColorPickerModule
  ],
  providers: [
    BuilderService,
    FormReaderService
  ],
  exports: [
    BuilderComponent
  ]
})
export class BuilderModule {
}
