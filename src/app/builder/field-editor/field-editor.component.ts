import { Component, Input, OnInit } from '@angular/core';
import { AbstractSideMenuComponent } from '../../shared/components/side-menu/side-menu.component';
import { BuilderService } from '../builder.service';

@Component({
  selector: 'app-field-editor',
  templateUrl: './field-editor.component.html',
  styleUrls: ['./field-editor.component.sass']
})
export class FieldEditorComponent extends AbstractSideMenuComponent implements OnInit {
  @Input() fieldModel: any;
  private backup: any;

  constructor(private builderServiceInstance: BuilderService) {
    super(builderServiceInstance);
  }

  ngOnInit() {
    this.backup = Object.assign(this.fieldModel);
  }

  removeField() {
    this.fieldModel._ref.destroy();
    this.builderServiceInstance.setSelectedField(null);
  }
}
