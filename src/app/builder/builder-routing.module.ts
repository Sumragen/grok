import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { builderRoutes } from './builder-routes';

@NgModule({
  imports: [
    RouterModule.forChild(builderRoutes)
  ]
})
export class BuilderRoutingModule {
}
