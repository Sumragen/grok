import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import {environment} from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { AppComponent } from './app.component';
import { BuilderModule } from './builder/builder.module';
import { DbService } from './shared/services/db.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    BuilderModule,
    RouterModule.forRoot([]),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  exports: [
    RouterModule
  ],
  providers: [DbService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
