# Grok

## Dynamic form builder

### User have ability to:

1. Change form color
1. Change background color
1. Application have 2 work mode - "Constructor" and "Preview". In the first one user can attach different types of control fields to form. And the second one - show view which will be available for every one who have link.

Unfortunately, we have not implemented view mode by link
